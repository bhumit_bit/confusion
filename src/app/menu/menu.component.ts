import { Component, OnInit } from '@angular/core';
import { Dish } from '../shared/dish';
//import { DISHES } from '../shared/dishes'; using service instead

import { DishService } from '../services/dish.service';

//const DISHES: Dish[] = [
//earlier whole data was added here then moved to shared/dishes.ts

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})





export class MenuComponent implements OnInit {

	//dishes: Dish[] = DISHES;  using service to get data for dishes
  //selectedDish = DISHES[0];
  dishes: Dish[];
  selectedDish: Dish;

  constructor(private dishService: DishService) { }

  ngOnInit() { // it is a life cycle method (need to find more)

    this.dishes = this.dishService.getDishes();
  }

  onSelect(dish: Dish){
    this.selectedDish = dish;
  }

}
