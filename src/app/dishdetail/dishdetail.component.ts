import { Component, OnInit, Input } from '@angular/core';
import { Dish } from '../shared/dish';


@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss']
})

export class DishdetailComponent implements OnInit {

  @Input()
  dish: Dish;
  // the Inpur decorator will allow to accpet and use the value
  // in variable [dish] = "selectedDish" passed from menu component
  // to disdetailcomponent


  //comment = COMMENTS;

  constructor() { }

  ngOnInit(): void {
  }

}
