import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';

import 'hammerjs';
import { MenuComponent } from './menu/menu.component'; // this will be added by CLI when creating the component (same like AppComponent)
import { MatListModule } from '@angular/material/list'; // removed for creating new mendu with GirdList
import { MatGridListModule } from '@angular/material/grid-list';
import { DishdetailComponent } from './dishdetail/dishdetail.component';
import { MatCardModule } from '@angular/material/card';
//import { MatButtonModule } from '@angular/material/button';

import { DishService } from './services/dish.service'; //injecting the dependency

// as mentioned in viedos
// this is a decorator which inclucdes all the package which will be used
// user will have to enter required packages in "imports"
@NgModule({
  declarations: [ //this will have list of components which will appear here when component is created via CLI
    AppComponent,
    MenuComponent,
    DishdetailComponent
  ],
  imports: [
    BrowserModule,
	BrowserAnimationsModule,
    MatToolbarModule,
    FlexLayoutModule,
	MatListModule, // removed for creating new mendu with GirdList
  MatGridListModule,
 MatCardModule
//  MatButtonModule
  ],
  providers: [
    DishService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
